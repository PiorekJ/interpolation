﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Interpolation.Core;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using InputManager = Interpolation.Core.InputManager;

namespace Interpolation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Simulation Simulation { get; set; }


        public MainWindow()
        {
            Simulation = new Simulation();
            InitializeComponent();
        }

        private void DisplayControl_OnControlLeftLoaded(object sender, RoutedEventArgs e)
        {
            Simulation.InitializeSimulationE();
            DisplayControlLeft.MakeCurrent();
            Simulation.Scene.Camera.SetViewportValues(DisplayControlLeft.AspectRatio, DisplayControlLeft.ControlWidth, DisplayControlLeft.ControlHeight);
            GL.Viewport(0, 0, DisplayControlLeft.ControlWidth, DisplayControlLeft.ControlHeight);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.VertexProgramPointSize);

            CompositionTarget.Rendering += OnRender;
        }

        private void DisplayControl_OnControlRightLoaded(object sender, RoutedEventArgs e)
        {
            Simulation.InitializeSimulationQ();
            DisplayControlRight.MakeCurrent();
            Simulation.Scene.Camera.SetViewportValues(DisplayControlRight.AspectRatio, DisplayControlRight.ControlWidth, DisplayControlRight.ControlHeight);
            GL.Viewport(0, 0, DisplayControlRight.ControlWidth, DisplayControlRight.ControlHeight);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.VertexProgramPointSize);
        }

        private void OnRender(object sender, EventArgs e)
        {
            Simulation.Scene.Camera.UpdateCamera(DisplayControlLeft.MousePosition);
            var dt = Simulation.TimeCounter.CountDT();

            DisplayControlLeft.MakeCurrent();
            GL.ClearColor(System.Drawing.Color.Gray);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            DrawEulerScene();
            DisplayControlLeft.SwapBuffers();

            DisplayControlRight.MakeCurrent();
            GL.ClearColor(System.Drawing.Color.Gray);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            DrawQuaternionScene();
            DisplayControlRight.SwapBuffers();
        }

        private void DrawEulerScene()
        {
            Simulation.Scene.SceneBackground.Render();
            Simulation.Scene.ESceneCursorStart.Update();
            Simulation.Scene.ESceneCursorStart.Render();
            Simulation.Scene.ESceneCursorEnd.Update();
            Simulation.Scene.ESceneCursorEnd.Render();

            foreach (Model sceneObject in Simulation.Scene.SceneObjectsE)
            {
                sceneObject.Update();
                sceneObject.Render();
            }
        }

        private void DrawQuaternionScene()
        {
            Simulation.Scene.SceneBackground.Render();
            Simulation.Scene.QSceneCursorStart.Update();
            Simulation.Scene.QSceneCursorStart.Render();
            Simulation.Scene.QSceneCursorEnd.Update();
            Simulation.Scene.QSceneCursorEnd.Render();

            foreach (Model sceneObject in Simulation.Scene.SceneObjectsQ)
            {
                sceneObject.Update();
                sceneObject.Render();
            }
        }

        private void DisplayControl_OnControlResized(object sender, RoutedEventArgs e)
        {
            DisplayControlLeft.MakeCurrent();
            Simulation.Scene.Camera.SetViewportValues(DisplayControlLeft.AspectRatio, DisplayControlLeft.ControlWidth, DisplayControlLeft.ControlHeight);
            GL.Viewport(0, 0, DisplayControlLeft.ControlWidth, DisplayControlLeft.ControlHeight);
            DisplayControlRight.MakeCurrent();
            Simulation.Scene.Camera.SetViewportValues(DisplayControlRight.AspectRatio, DisplayControlRight.ControlWidth, DisplayControlRight.ControlHeight);
            GL.Viewport(0, 0, DisplayControlRight.ControlWidth, DisplayControlRight.ControlHeight);
        }

        private void DisplayControl_OnControlUnloaded(object sender, RoutedEventArgs e)
        {
            foreach (Model item in Simulation.Scene.SceneObjectsE)
            {
                item.Dispose();
            }

            foreach (Model item in Simulation.Scene.SceneObjectsQ)
            {
                item.Dispose();
            }
        }

        private void DisplayControl_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Simulation.Scene.Camera.SetLastMousePosition(DisplayControlLeft.MousePosition);
            InputManager.OnMouseButtonChange(e.ChangedButton, true);
        }

        private void DisplayControl_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            InputManager.OnMouseButtonChange(e.ChangedButton, false);
        }

        private void DisplayControl_OnKeyDown(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, true);
        }

        private void DisplayControl_OnKeyUp(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, false);
        }

        private void DisplayControl_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            InputManager.OnMouseScroll(e.Delta);
        }


    }
}
