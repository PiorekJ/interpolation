﻿#version 330 core

in layout(location = 0) vec3 position;
in layout(location = 1) vec3 color;

out VS_OUT 
{
	vec3 color;
} Out;

void main()
{
	gl_Position = vec4(position.xy, 0.0f, 1.0f);
	Out.color = color;
}
