﻿#version 330 core

out layout(location = 0) vec4 FragColor;

in V_Out
{
	vec3 normal;
	vec3 fragPos;
	vec3 color;
} In;

uniform float alpha;
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 camPos;
uniform float ks, kd, ka, m;


vec4 phong(vec3 worldPos, vec3 norm, vec3 view)
{
	view = normalize(view);
	norm = normalize(norm);
	vec3 colorAmbient = In.color * ka; //ambient
	vec3 lightVec = normalize(lightPos - worldPos);
	vec3 halfVec = normalize(view + lightVec);
	colorAmbient += lightColor * kd * colorAmbient * clamp(dot(norm, lightVec), 0.0, 1.0);//diffuse
	colorAmbient += lightColor * ks * pow(clamp(dot(norm, halfVec), 0.0, 1.0), m);//specular
	return clamp(vec4(colorAmbient, 1.0f), 0.0, 1.0);
}

void main()
{
	FragColor = phong(In.fragPos, In.normal, normalize(camPos - In.fragPos));
}
