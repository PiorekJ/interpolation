﻿#version 330 core

out layout(location = 0) vec4 FragColor;

uniform vec3 color;

void main()
{
	FragColor = vec4(color, 1.0f);
}
