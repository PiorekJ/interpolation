﻿using System;
using Interpolation.Core;
using Interpolation.Utils.Math;
using OpenTK;

namespace Interpolation.Components
{
    public class Transform : Component, IDisposable
    {
        private Vector3 _position = Vector3.Zero;
        private Quaternion _rotationQ = Quaternion.Identity;
        private Vector3 _rotationE = Vector3.Zero;
        private Vector3 _scale = Vector3.One;

        public delegate void UpdateEventHandler();

        private event UpdateEventHandler OnPositionUpdate;
        private event UpdateEventHandler OnRotationUpdate;
        private event UpdateEventHandler OnScaleUpdate;

        public Vector3 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                RaisePropertyChanged();
                OnPositionUpdate?.Invoke();
            }
        }

        public Quaternion RotationQ
        {
            get { return _rotationQ; }
            set
            {
                _rotationQ = value;
                RaisePropertyChanged();
                OnRotationUpdate?.Invoke();
            }
        }

        public Vector3 RotationE
        {
            get { return _rotationE; }
            set
            {
                _rotationE = value;
                RaisePropertyChanged();
                OnRotationUpdate?.Invoke();
            }
        }

        public Vector3 Scale
        {
            get { return _scale; }
            set
            {
                _scale = value;
                RaisePropertyChanged();
                OnScaleUpdate?.Invoke();
            }
        }

        private Matrix4 _rotationX = Matrix4.Identity;
        private Matrix4 _rotationY = Matrix4.Identity;
        private Matrix4 _rotationZ = Matrix4.Identity;
        public Matrix4 GetModelMatrixQ()
        {
            return MathGeneralUtils.CreateScale(Scale) * MathGeneralUtils.CreateFromQuaternion(RotationQ.Normalized()) * MathGeneralUtils.CreateTranslation(Position);
        }

        public Matrix4 GetModelMatrixE()
        {
            MathGeneralUtils.CreateRotationZ(MathHelper.DegreesToRadians(RotationE.Z), out _rotationZ);
            MathGeneralUtils.CreateRotationY(MathHelper.DegreesToRadians(RotationE.Y), out _rotationY);
            MathGeneralUtils.CreateRotationX(MathHelper.DegreesToRadians(RotationE.X), out _rotationX);
            return MathGeneralUtils.CreateScale(Scale) *  _rotationX * _rotationY * _rotationZ * MathGeneralUtils.CreateTranslation(Position);
        }

        public Transform()
        { }

        public Transform(UpdateEventHandler onPositionUpdate, UpdateEventHandler onRotationUpdate, UpdateEventHandler onScaleUpdate)
        {
            OnPositionUpdate += onPositionUpdate;
            OnRotationUpdate += onRotationUpdate;
            OnScaleUpdate += onScaleUpdate;
        }

        public void Dispose()
        {

        }
    }
}
