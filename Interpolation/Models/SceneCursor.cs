﻿using System.Collections.Generic;
using System.Windows.Media;
using Interpolation.Components;
using Interpolation.Core;
using Interpolation.OpenTK;
using Interpolation.Utils.OpenTK;
using OpenTK;
using PureCAD.Utils;

namespace Interpolation.Models
{
    public abstract class SceneCursor : Model
    {
        protected SceneCursor(Color color)
        {
            Transform.Position = Vector3.Zero;
            Shader = Shaders.LitObjectShader;
            Mesh = MeshGenerator.GenerateCursorCubeMesh(0.025f, 0.5f, 0.05f, color);
            IsVisibleOnList = false;
        }

        protected override void OnRender()
        {
            Shader.Use();
            if (this is ESceneCursor)
            {
                Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrixE());
                Shader.Bind(Shader.GetUniformLocation("lightColor"), Simulation.Scene.SceneLightE.Color.ColorToVector3());
                Shader.Bind(Shader.GetUniformLocation("lightPos"), Simulation.Scene.SceneLightE.Transform.Position);
            }
            else
            {
                Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrixQ());
                Shader.Bind(Shader.GetUniformLocation("lightColor"), Simulation.Scene.SceneLightQ.Color.ColorToVector3());
                Shader.Bind(Shader.GetUniformLocation("lightPos"), Simulation.Scene.SceneLightQ.Transform.Position);
            }
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("alpha"), 1.0f);
            
            Shader.Bind(Shader.GetUniformLocation("camPos"), Simulation.Scene.Camera.Transform.Position);
            Shader.Bind(Shader.GetUniformLocation("ks"), 0.3f);
            Shader.Bind(Shader.GetUniformLocation("kd"), 0.75f);
            Shader.Bind(Shader.GetUniformLocation("ka"), 0.5f);
            Shader.Bind(Shader.GetUniformLocation("m"), 100.0f);
            Mesh.Draw();

            //Shader.Use();
            //if(this is ESceneCursor)
            //    Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrixE());
            //else
            //    Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrixQ());
            //Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            //Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            //Mesh.Draw();
        }

        protected override void OnUpdate()
        {

        }

        private Mesh<VertexPC> GenerateCursorMesh()
        {
            List<VertexPC> vertices = new List<VertexPC>();
            vertices.Add(new VertexPC(Vector3.Zero, Vector3.UnitX));
            vertices.Add(new VertexPC(Vector3.UnitX * 0.5f, Vector3.UnitX));
            vertices.Add(new VertexPC(Vector3.Zero, Vector3.UnitY));
            vertices.Add(new VertexPC(Vector3.UnitY * 0.5f, Vector3.UnitY));
            vertices.Add(new VertexPC(Vector3.Zero, Vector3.UnitZ));
            vertices.Add(new VertexPC(Vector3.UnitZ * 0.5f, Vector3.UnitZ));

            return new Mesh<VertexPC>(vertices, null, MeshType.Lines, AccessType.Static);
        }
    }
}
