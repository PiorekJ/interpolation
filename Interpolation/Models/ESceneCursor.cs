﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using Interpolation.Components;
using Interpolation.Core;
using Interpolation.OpenTK;
using Interpolation.Utils.Math;
using OpenTK;

namespace Interpolation.Models
{
    public class ESceneCursor : SceneCursor
    {
        private bool _isStatic;
        private float _currentTime = 0.0f;
        private Vector3 startRadians;
        private Vector3 endRadians;
        public float CurrentTime
        {
            get { return _currentTime; }
            set
            {
                _currentTime = value;
                RaisePropertyChanged();
            }
        }

        public ESceneCursor(bool isStatic, Color color) : base(color)
        {
            _isStatic = isStatic;
        }

        public void Start()
        {
            float normSX = NormalizeAngle360(Simulation.Settings.StartRotationE.X);
            float normSY = NormalizeAngle360(Simulation.Settings.StartRotationE.Y);
            float normSZ = NormalizeAngle360(Simulation.Settings.StartRotationE.Z);

            float normEX = NormalizeAngle360(Simulation.Settings.EndRotationE.X);
            float normEY = NormalizeAngle360(Simulation.Settings.EndRotationE.Y);
            float normEZ = NormalizeAngle360(Simulation.Settings.EndRotationE.Z);

            if (normSX - normEX > 180)
                normSX -= 360;
            if (normSX - normEX <= -180)
                normEX -= 360;

            if (normSY - normEY > 180)
                normSY -= 360;
            if (normSY - normEY <= -180)
                normEY -= 360;

            if (normSZ - normEZ > 180)
                normSZ -= 360;
            if (normSZ - normEZ <= -180)
                normEZ -= 360;
            //if (Simulation.Settings.StartRotationE.X - Simulation.Settings.EndRotationE.X > 180)
            //    deltaX = Simulation.Settings.EndRotationE.X + (int)((Simulation.Settings.EndRotationE.X / 360) + 1) * 360;
            //if (Simulation.Settings.StartRotationE.X - Simulation.Settings.EndRotationE.X < -180)
            //    deltaX = Simulation.Settings.EndRotationE.X - (int)((Simulation.Settings.EndRotationE.X / 360) + 1) * 360;
            //
            //if (Simulation.Settings.StartRotationE.Y - Simulation.Settings.EndRotationE.Y > 180)
            //    deltaY = Simulation.Settings.EndRotationE.Y + (int)((Simulation.Settings.EndRotationE.Y / 360) + 1) * 360;
            //if (Simulation.Settings.StartRotationE.Y - Simulation.Settings.EndRotationE.Y < -180)
            //    deltaY = Simulation.Settings.EndRotationE.Y - (int)((Simulation.Settings.EndRotationE.Y / 360) + 1) * 360;
            //
            //if (Simulation.Settings.StartRotationE.Z - Simulation.Settings.EndRotationE.Z > 180)
            //    deltaZ = Simulation.Settings.EndRotationE.Z + (int)((Simulation.Settings.EndRotationE.Z / 360) + 1) * 360;
            //if (Simulation.Settings.StartRotationE.Z - Simulation.Settings.EndRotationE.Z < -180)
            //    deltaZ = Simulation.Settings.EndRotationE.Z - (int)((Simulation.Settings.EndRotationE.Z / 360) + 1) * 360;

            //var endClamped = new Vector3(Single.IsNaN(deltaX) ? Simulation.Settings.EndRotationE.X : deltaX, Single.IsNaN(deltaY) ? Simulation.Settings.EndRotationE.Y : deltaY, Single.IsNaN(deltaZ) ? Simulation.Settings.EndRotationE.Z : deltaZ);
            startRadians = new Vector3(MathHelper.DegreesToRadians(normSX), MathHelper.DegreesToRadians(normSY), MathHelper.DegreesToRadians(normSZ));
            endRadians = new Vector3(MathHelper.DegreesToRadians(normEX), MathHelper.DegreesToRadians(normEY), MathHelper.DegreesToRadians(normEZ));
        }

        private float NormalizeAngle360(float angle)
        {
            while (angle > 360)
                angle -= 360;
            while (angle <= 0)
                angle += 360;

            return angle;
        }

        protected override void OnUpdate()
        {
            if (_isStatic)
                return;

            if (Simulation.Settings.IsRunning)
            {
                if (CurrentTime < Simulation.Settings.AnimationTime)
                {
                    CalculateStepAt(CurrentTime / Simulation.Settings.AnimationTime);
                    CurrentTime += Simulation.DeltaTime;
                }
                else
                {
                    Transform.Position = Simulation.Settings.EndPosition;
                    Transform.RotationE = Simulation.Settings.EndRotationE;
                }
            }

        }

        public void CalculateStepAt(float t)
        {
            Transform.Position = MathGeneralUtils.Lerp(Simulation.Settings.StartPosition,
                Simulation.Settings.EndPosition, t);
            //Transform.RotationE = MathGeneralUtils.Lerp(Simulation.Settings.StartRotationE,
            //    Simulation.Settings.EndRotationE, CurrentTime / Simulation.Settings.AnimationTime);

            var lerp = MathGeneralUtils.Lerp(startRadians, endRadians, t);

            //var lerpX = MathGeneralUtils.Lerp(startRadians.X, endRadians.X, t);
            //var lerpY = MathGeneralUtils.Lerp(startRadians.Y, endRadians.Y, t);
            //var lerpZ = MathGeneralUtils.Lerp(startRadians.Z, endRadians.Z, t);
            Transform.RotationE = new Vector3(MathHelper.RadiansToDegrees(lerp.X), MathHelper.RadiansToDegrees(lerp.Y), MathHelper.RadiansToDegrees(lerp.Z));
        }

        public void Reset()
        {
            CurrentTime = 0.0f;
        }

        
    }
}
