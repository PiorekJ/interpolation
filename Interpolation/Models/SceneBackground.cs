﻿using System.Collections.Generic;
using Interpolation.Core;
using Interpolation.OpenTK;
using Interpolation.Utils;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Utils;
using Color = System.Windows.Media.Color;
using ColorConverter = System.Windows.Media.ColorConverter;

namespace Interpolation.Models
{
    public class SceneBackground : Model
    {

        public SceneBackground()
        {
            Shader = Shaders.BasicQuadShader;
            Mesh = GenerateGradientBackground(20.0f, ((Color)ColorConverter.ConvertFromString("#FF424242")).ColorToVector3(), Constants.BackgroundColor.ColorToVector3(), ((Color)ColorConverter.ConvertFromString("#FF424242")).ColorToVector3());
        }

        private Mesh<VertexPC> GenerateGradientBackground(float stripPercent, Vector3 topColor, Vector3 middleColor, Vector3 bottomColor)
        {
            stripPercent.Clamp(0, 50);
            var vertices = new List<VertexPC>();
            var edges = new List<uint>();

            vertices.Add(new VertexPC(-1, 1, 0, topColor));
            vertices.Add(new VertexPC(1, 1, 0, topColor));
            vertices.Add(new VertexPC(-1, 1 - (1 * stripPercent/100.0f), 0, middleColor));
            vertices.Add(new VertexPC(1, 1 - (1 * stripPercent / 100.0f), 0, middleColor));

            vertices.Add(new VertexPC(-1, -1 + (1* stripPercent/100.0f), 0, middleColor));
            vertices.Add(new VertexPC(1, -1 + (1 * stripPercent / 100.0f), 0, middleColor));
            vertices.Add(new VertexPC(-1, -1, 0, bottomColor));
            vertices.Add(new VertexPC(1, -1, 0, bottomColor));

            edges.Add(0);
            edges.Add(2);
            edges.Add(1);

            edges.Add(2);
            edges.Add(3);
            edges.Add(1);

            edges.Add(2);
            edges.Add(3);
            edges.Add(4);

            edges.Add(4);
            edges.Add(5);
            edges.Add(3);

            edges.Add(4);
            edges.Add(6);
            edges.Add(5);

            edges.Add(6);
            edges.Add(7);
            edges.Add(5);


            return new Mesh<VertexPC>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }



        protected override void OnRender()
        {
            GL.Disable(EnableCap.DepthTest);

            Shader.Use();
            Mesh.Draw();

            GL.Enable(EnableCap.DepthTest);
        }
    }
}
