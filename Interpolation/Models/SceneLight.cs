﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Interpolation.Core;
using Interpolation.Utils.OpenTK;
using OpenTK;
using PureCAD.Utils;

namespace Interpolation.Models
{
    public class SceneLight : Model
    {
        public Color Color { get; set; } = Colors.White;
        public int LightIntensity = 300;
        public SceneLight()
        {
            //Shader = Shaders.LightShader;
            //Mesh = MeshGenerator.GenerateCubeMesh(0.25f, 0.25f, 0.25f);
            Transform.Position = Vector3.UnitY * 10;
        }

        protected override void OnRender()
        {
            //Shader.Use();
            //Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrixQ());
            //Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            //Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            //Shader.Bind(Shader.GetUniformLocation("color"), Color.ColorToVector3());
            //Mesh.Draw();
        }
    }
}
