﻿using System.Collections.Generic;
using System.Windows.Media;
using Interpolation.Components;
using Interpolation.Core;
using Interpolation.OpenTK;
using Interpolation.Utils;
using Interpolation.Utils.Math;
using OpenTK;
using Color = System.Windows.Media.Color;

namespace Interpolation.Models
{
    public class QSceneCursor : SceneCursor
    {
        private bool _isStatic;
        private float _currentTime = 0.0f;

        public float CurrentTime
        {
            get { return _currentTime; }
            set
            {
                _currentTime = value; 
                RaisePropertyChanged();
            }
        }

        public QSceneCursor(bool isStatic, Color color) : base(color)
        {
            _isStatic = isStatic;
        }

        protected override void OnUpdate()
        {
            if (_isStatic)
                return;

            if (Simulation.Settings.IsRunning)
            {
                if (CurrentTime < Simulation.Settings.AnimationTime)
                {
                    CalculateStepAt(CurrentTime / Simulation.Settings.AnimationTime);
                    CurrentTime += Simulation.DeltaTime;
                }
                else
                {
                    Transform.Position = Simulation.Settings.EndPosition;
                    Transform.RotationQ = Simulation.Settings.EndRotationQ;
                }
            }

        }

        public void CalculateStepAt(float t)
        {
            Transform.Position = MathGeneralUtils.Lerp(Simulation.Settings.StartPosition,
                Simulation.Settings.EndPosition, t);

            if (Simulation.Settings.CurrentQuaternionRotationType == Types.QuaternionRotationTypes.LERP)
            {
                Transform.RotationQ = MathGeneralUtils.Lerp(Simulation.Settings.StartRotationQ,
                    Simulation.Settings.EndRotationQ, t);
            }
            if (Simulation.Settings.CurrentQuaternionRotationType == Types.QuaternionRotationTypes.SLERP)
            {
                Transform.RotationQ = MathGeneralUtils.SLerp(Simulation.Settings.StartRotationQ,
                    Simulation.Settings.EndRotationQ, t);
            }
        }

        public void Reset()
        {
            CurrentTime = 0.0f;
        }
    }
}
