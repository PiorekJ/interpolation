﻿using Interpolation.Components;
using Interpolation.Utils;
using Interpolation.Utils.Math;
using OpenTK;
using Key = System.Windows.Input.Key;
using MouseButton = System.Windows.Input.MouseButton;

namespace Interpolation.Core
{
    public class Camera : SceneObject
    {
        public float FOV { get; set; } = Constants.DefaultFOV;
        public float ZNear { get; set; } = Constants.DefaultFOV;
        public float ZFar { get; set; } = Constants.DefaultZFar;

        private Vector2 _lastMousePosition = Vector2.Zero;

        public float RotationX
        {
            get { return _rotationX; }
            set
            {
                _rotationX = value;
                RaisePropertyChanged();
            }
        }

        public float RotationY
        {
            get { return _rotationY; }
            set
            {
                _rotationY = value;
                RaisePropertyChanged();
            }
        }


        private float _aspectRatio;
        private float _rotationX;
        private float _rotationY;
        private bool _isMoving;
        public bool IsMouseMoving => InputManager.IsMouseButtonDown(MouseButton.Middle);
        public bool IsKeyboardMoving => InputManager.IsKeyDown(Key.W) || InputManager.IsKeyDown(Key.S) || InputManager.IsKeyDown(Key.A) || InputManager.IsKeyDown(Key.D) || InputManager.IsKeyDown(Key.Space) || InputManager.IsKeyDown(Key.LeftCtrl);

        public bool IsRotating => InputManager.IsMouseButtonDown(MouseButton.Right);

        public bool IsIsometric = false;

        public Matrix4 GetViewMatrix()
        {
            return MathGeneralUtils.CreateTranslation(-Transform.Position) * MathGeneralUtils.CreateFromQuaternion(Transform.RotationQ.Inverted());
        }

        public Matrix4 GetProjectionMatrix()
        {
            return MathGeneralUtils.CreatePerspectiveFOVMatrix(FOV, _aspectRatio, ZNear, ZFar);
        }

        public void SetViewportValues(float aspectRatio, int width, int height)
        {
            _aspectRatio = aspectRatio;
        }

        public Camera()
        {
            Transform.Position = Constants.DefaultCameraPosition;
            InputManager.OnMouseScrollEvent += Zoom;
            InputManager.RegisterOnKeyDownEvent(Key.NumPad5, () =>
            {
                IsIsometric = !IsIsometric;
            });
            InputManager.RegisterOnKeyDownEvent(Key.NumPad1, () =>
            {
                if (!IsIsometric)
                    IsIsometric = true;
                RotationX = 0;
                RotationY = 0;
                Transform.RotationQ = Quaternion.FromAxisAngle(Vector3.UnitY, RotationY) *
                                     Quaternion.FromAxisAngle(Vector3.UnitX, RotationX);
                Transform.Position = Vector3.UnitZ;
            });
            InputManager.RegisterOnKeyDownEvent(Key.NumPad3, () =>
            {
                if (!IsIsometric)
                    IsIsometric = true;
                RotationX = 0;
                RotationY = MathHelper.DegreesToRadians(90);
                Transform.RotationQ = Quaternion.FromAxisAngle(Vector3.UnitY, RotationY) *
                                     Quaternion.FromAxisAngle(Vector3.UnitX, RotationX);
                Transform.Position = Vector3.UnitX;

            });
            InputManager.RegisterOnKeyDownEvent(Key.NumPad7, () =>
            {
                if (!IsIsometric)
                    IsIsometric = true;
                RotationX = MathHelper.DegreesToRadians(-90);
                RotationY = 0;
                Transform.RotationQ = Quaternion.FromAxisAngle(Vector3.UnitY, RotationY) *
                                     Quaternion.FromAxisAngle(Vector3.UnitX, RotationX);
                Transform.Position = Vector3.UnitY;
            });
        }

        public void UpdateCamera(Vector2 mousePosition)
        {
            KeyboardControl();
            MouseControl(mousePosition);
        }

        private void MouseControl(Vector2 mousePosition)
        {
            if (_lastMousePosition == mousePosition)
                return;

            var diffX = mousePosition.X - _lastMousePosition.X;
            var diffY = mousePosition.Y - _lastMousePosition.Y;

            _lastMousePosition = mousePosition;

            if (IsMouseMoving)
            {
                Vector3 top = Vector3.Transform(-Vector3.UnitY, Transform.RotationQ);
                Vector3 right = Vector3.Transform(Vector3.UnitX, Transform.RotationQ);

                Transform.Position -= (top * diffY + right * diffX) * Constants.CameraMovementMouseSensitivity * Simulation.DeltaTime;
            }

            if (IsRotating)
            {
                RotationX -= diffY * Constants.CameraRotationMouseSensitivity * Simulation.DeltaTime;
                RotationY -= diffX * Constants.CameraRotationMouseSensitivity * Simulation.DeltaTime;

                Transform.RotationQ = Quaternion.FromAxisAngle(Vector3.UnitY, RotationY) *
                           Quaternion.FromAxisAngle(Vector3.UnitX, RotationX);
            }
        }

        private void Zoom(int delta)
        {
            if (delta < 0)
                Transform.Position += Transform.RotationQ * Vector3.UnitZ * Constants.CameraZoomMouseSensitivity;
            if (delta > 0)
                Transform.Position -= Transform.RotationQ * Vector3.UnitZ * Constants.CameraZoomMouseSensitivity;
        }

        private void KeyboardControl()
        {
            float velocity = InputManager.IsKeyDown(Key.LeftShift) ? Constants.CameraMovementKeySlowVelocity : Constants.CameraMovementKeyVelocity;

            if (InputManager.IsKeyDown(Key.W))
            {
                Transform.Position -= Transform.RotationQ * Vector3.UnitZ * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.S))
            {
                Transform.Position += Transform.RotationQ * Vector3.UnitZ * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.A))
            {
                Transform.Position -= Transform.RotationQ * Vector3.UnitX * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.D))
            {
                Transform.Position += Transform.RotationQ * Vector3.UnitX * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.Space))
            {
                Transform.Position += Transform.RotationQ * Vector3.UnitY * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.LeftCtrl))
            {
                Transform.Position -= Transform.RotationQ * Vector3.UnitY * velocity * Simulation.DeltaTime;
            }


        }

        public void SetLastMousePosition(Vector2 pos)
        {
            _lastMousePosition = pos;
        }

        protected override void OnRender() { }

        protected override void OnUpdate() { }
    }
}
