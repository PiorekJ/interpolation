﻿using Interpolation.Utils.UIExtensionClasses;

namespace Interpolation.Core
{
    public abstract class Component : BindableObject
    {
        public SceneObject Owner;

        protected Component()
        {
        }
    }
}
