﻿using System.Windows.Input;
using Interpolation.Utils;
using Interpolation.Utils.UIExtensionClasses;

namespace Interpolation.Core
{
    public class Simulation
    {
        public static Scene Scene { get; set; }
        public static Settings Settings { get; set; }

        public TimeCounter TimeCounter;

        public static float DeltaTime => TimeCounter.DeltaTime;

        #region Commands

        public ICommand StartCommand { get; set; }
        public ICommand PauseCommand { get; set; }
        public ICommand StopCommand { get; set; }

        #endregion

        public Simulation()
        {
            Scene = new Scene();
            Settings = new Settings();
            TimeCounter = new TimeCounter();
            StartCommand = new RelayCommand(param => StartSimulation());
            PauseCommand = new RelayCommand(param => PauseSimulation());
            StopCommand = new RelayCommand(param => StopSimulation());
            TimeCounter.Start();
        }

        private void StartSimulation()
        {
            if (Settings.DisplaySteps)
                return;

            Settings.UIEnabled = false;
            Scene.ESceneCursorStart.Start();
            Settings.IsRunning = true;
        }

        private void PauseSimulation()
        {
            Settings.IsRunning = false;
        }

        private void StopSimulation()
        {
            Settings.UIEnabled = true;
            Settings.IsRunning = false;
            Scene.ESceneCursorStart.Transform.Position = Settings.StartPosition;
            Scene.QSceneCursorStart.Transform.Position = Settings.StartPosition;
            Scene.ESceneCursorStart.Transform.RotationE = Settings.StartRotationE;
            Scene.QSceneCursorStart.Transform.RotationQ = Settings.StartRotationQ;
            Scene.ESceneCursorStart.Reset();
            Scene.QSceneCursorStart.Reset();
        }


        public void InitializeSimulationE()
        {
            Scene.AddSceneLightE();
            Scene.AddSceneCursorE();
            Scene.AddSceneBackground();
        }

        public void InitializeSimulationQ()
        {
            Scene.AddSceneLightQ();
            Scene.AddSceneCursorQ();
            Scene.AddSceneBackground();
        }
    }
}
