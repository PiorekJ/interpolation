﻿using System;
using System.Windows.Input;
using OpenTK;

namespace Interpolation.Core
{
    public static class InputManager
    {
        private static InputMethod[] _keysState;
        private static InputMethod[] _mouseState;
        private static Vector2 _mousePosition;

        public static Vector2 MousePosition
        {
            get { return _mousePosition; }
        }

        public delegate void MouseScrollEventHandler(int delta);
        public static event MouseScrollEventHandler OnMouseScrollEvent;

        public delegate void InputEventHandler();

        static InputManager()
        {
            _keysState = new InputMethod[Enum.GetNames(typeof(Key)).Length];
            for (var i = 0; i < Enum.GetNames(typeof(Key)).Length; i++)
            {
                _keysState[i] = new InputMethod(i);
            }

            _mouseState = new InputMethod[Enum.GetNames(typeof(MouseButton)).Length];
            for (var i = 0; i < Enum.GetNames(typeof(MouseButton)).Length; i++)
            {
                _mouseState[i] = new InputMethod(i);
            }
        }

        public static Vector2 SetMousePosition(Vector2 pos)
        {
            _mousePosition = pos;
            return _mousePosition;
        }

        public static void OnKeyChange(Key key, bool state)
        {
            _keysState[(int)key].IsPressed = state;
        }

        public static void OnMouseButtonChange(MouseButton button, bool state)
        {
            _mouseState[(int)button].IsPressed = state;
        }

        public static void OnMouseScroll(int delta)
        {
            OnMouseScrollEvent?.Invoke(delta);
        }

        public static bool IsKeyDown(Key key)
        {
            return _keysState[(int)key].IsPressed;
        }

        public static bool IsMouseButtonDown(MouseButton key)
        {
            return _mouseState[(int)key].IsPressed;
        }

        public static void RegisterOnKeyDownEvent(Key key, InputEventHandler handler)
        {
            _keysState[(int)key].RegisterOnKeyDownEvent(handler);
        }

        public static void DeegisterOnKeyDownEvent(Key key, InputEventHandler handler)
        {
            _keysState[(int)key].DeegisterOnKeyDownEvent(handler);
        }

        public static void RegisterOnKeyUpEvent(Key key, InputEventHandler handler)
        {
            _keysState[(int)key].RegisterOnKeyUpEvent(handler);
        }

        public static void DeegisterOnKeyUpEvent(Key key, InputEventHandler handler)
        {
            _keysState[(int)key].DeegisterOnKeyUpEvent(handler);
        }

        private class InputMethod
        {
            public int KeyId;
            private bool _isPressed;

            public bool IsPressed
            {
                get => _isPressed;
                set
                {
                    _isPressed = value;
                    if (value)
                        OnKeyDown?.Invoke();
                    else
                        OnKeyUp?.Invoke();
                }
            }

            private event InputEventHandler OnKeyDown;
            private event InputEventHandler OnKeyUp;

            public InputMethod(int keyId)
            {
                KeyId = keyId;
                IsPressed = false;
            }

            public void RegisterOnKeyDownEvent(InputEventHandler handler)
            {
                OnKeyDown += handler;
            }

            public void DeegisterOnKeyDownEvent(InputEventHandler handler)
            {
                OnKeyDown -= handler;
            }

            public void RegisterOnKeyUpEvent(InputEventHandler handler)
            {
                OnKeyUp += handler;
            }

            public void DeegisterOnKeyUpEvent(InputEventHandler handler)
            {
                OnKeyUp -= handler;
            }
        }
    }
}

