﻿using System.Windows.Media;
using Interpolation.OpenTK;

namespace Interpolation.Core
{
    public interface IColorable
    {
        Color Color { get; set; }
    }
    
    public abstract class Model : SceneObject
    {
        public IMesh Mesh;
        public Shader Shader;
        public override void Dispose()
        {
            Mesh?.Dispose();
            base.Dispose();
        }


    }
}
