﻿using System;
using System.Collections.Generic;
using Interpolation.Components;
using Interpolation.Utils.UIExtensionClasses;

namespace Interpolation.Core
{
    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }

    public class SceneObject : BindableObject, IDisposable
    {
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                RaisePropertyChanged();
            }
        }

        public bool IsActive
        {
            get { return _isVisible; }
            set
            {
                _isActive = value;
                RaisePropertyChanged();
            }
        }

        public bool IsDeletable = true;

        public Transform Transform { get; set; }

        private List<Component> _components;

        public delegate void DeleteHandler();

        public event DeleteHandler OnDeleteEvent;

        #region UI Related

        public bool IsVisibleOnList
        {
            get { return _isVisibleOnList; }
            set
            {
                _isVisibleOnList = value;
                RaisePropertyChanged();
            }
        }
        private bool _isVisibleOnList = true;

        public string ObjectName
        {
            get => _objectName;
            set
            {
                _objectName = value;
                RaisePropertyChanged();
            }
        }

        private string _objectName;
        private bool _isVisible = true;
        private bool _isActive = true;
        #endregion

        public SceneObject()
        {
            _components = new List<Component>();
            Transform = AddComponent<Transform>();
        }



        public TComponent AddComponent<TComponent>()
            where TComponent : Component, new()
        {
            TComponent component = new TComponent { Owner = this };
            _components.Add(component);
            return component;
        }

        public TComponent GetComponent<TComponent>()
            where TComponent : Component
        {
            foreach (var item in _components)
            {
                if (item is TComponent)
                {
                    return (TComponent)item;
                }
            }
            return null;
        }

        public void RemoveComponent(Component component)
        {
            _components.Remove(component);
        }


        protected virtual void OnRender()
        {

        }

        protected virtual void OnUpdate()
        {

        }

        public void Render()
        {
            if (!IsVisible)
                return;

            OnRender();
        }

        public virtual void Update()
        {
            if (!IsActive)
                return;
            OnUpdate();
        }

        public void OnDelete()
        {
            OnDeleteEvent?.Invoke();
        }

        public virtual void Dispose()
        {
            Transform.Dispose();
            RemoveComponent(Transform);
        }
    }
}
