﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using Interpolation.Models;
using Interpolation.Utils.OpenTK;
using Interpolation.Utils.UIExtensionClasses;
using OpenTK;

namespace Interpolation.Core
{
    public class Scene : BindableObject
    {
        private ESceneCursor _eSceneCursorStart;
        private ESceneCursor _eSceneCursorEnd;
        private QSceneCursor _qSceneCursorStart;
        private QSceneCursor _qSceneCursorEnd;
        public Camera Camera { get; set; }

        public SceneLight SceneLightE { get; set; }
        public SceneLight SceneLightQ { get; set; }

        public ESceneCursor ESceneCursorStart
        {
            get { return _eSceneCursorStart; }
            set
            {
                _eSceneCursorStart = value;
                RaisePropertyChanged();
            }
        }

        public ESceneCursor ESceneCursorEnd
        {
            get { return _eSceneCursorEnd; }
            set
            {
                _eSceneCursorEnd = value;
                RaisePropertyChanged();
            }
        }

        public QSceneCursor QSceneCursorStart
        {
            get { return _qSceneCursorStart; }
            set
            {
                _qSceneCursorStart = value;
                RaisePropertyChanged();
            }
        }

        public QSceneCursor QSceneCursorEnd
        {
            get { return _qSceneCursorEnd; }
            set
            {
                _qSceneCursorEnd = value;
                RaisePropertyChanged();
            }
        }

        public SceneBackground SceneBackground { get; set; }

        public ObservableCollection<Model> SceneObjectsE { get; set; }
        public ObservableCollection<Model> SceneObjectsQ { get; set; }

        public Scene()
        {
            Camera = new Camera();
            SceneObjectsE = new ObservableCollection<Model>();
            SceneObjectsQ = new ObservableCollection<Model>();
        }

        public void AddSceneLightE()
        {
            SceneLightE = new SceneLight();
        }

        public void AddSceneLightQ()
        {
            SceneLightQ = new SceneLight();
        }

        public void AddSceneCursorE()
        {
            ESceneCursorStart = new ESceneCursor(false, Colors.Cyan);
            ESceneCursorEnd = new ESceneCursor(true, Colors.Yellow);
        }

        public ESceneCursor AddDummySceneCursorE(Color color)
        {
            var item = new ESceneCursor(true, color);
            SceneObjectsE.Add(item);
            return item;
        }

        public void AddSceneCursorQ()
        {
            QSceneCursorStart = new QSceneCursor(false, Colors.Cyan);
            QSceneCursorEnd = new QSceneCursor(true, Colors.Yellow);
        }

        public QSceneCursor AddDummySceneCursorQ(Color color)
        {
            var item = new QSceneCursor(true, color);
            SceneObjectsQ.Add(item);
            return item;
        }

        public SceneBackground AddSceneBackground()
        {
            SceneBackground = new SceneBackground();
            return SceneBackground;
        }

        public void RemoveSceneObjectE(Model item)
        {
            if (item != null)
            {
                SceneObjectsE.Remove(item);
                item.Dispose();
            }
        }

        public void RemoveSceneObjectQ(Model item)
        {
            if (item != null)
            {
                SceneObjectsQ.Remove(item);
                item.Dispose();
            }
        }

        public void RemoveSteps()
        {
            for (int i = SceneObjectsE.Count - 1; i >= 0; i--)
            {
                if (SceneObjectsE[i] is SceneCursor)
                    RemoveSceneObjectE(SceneObjectsE[i]);
            }

            for (int i = SceneObjectsQ.Count - 1; i >= 0; i--)
            {
                if (SceneObjectsQ[i] is SceneCursor)
                    RemoveSceneObjectQ(SceneObjectsQ[i]);
            }
        }

        public void AddSteps(int steps)
        {
            var step = 1.0f / steps;

            for (int i = 1; i < steps; i++)
            {
                var item = AddDummySceneCursorE(Colors.Yellow);
                item.Start();
                item.CalculateStepAt(i * step);
            }

            for (int i = 1; i < steps; i++)
            {
                var item = AddDummySceneCursorQ(Colors.Yellow);
                item.CalculateStepAt(i * step);
            }
        }
    }
}