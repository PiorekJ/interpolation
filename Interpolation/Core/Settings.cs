﻿using Interpolation.Utils;
using Interpolation.Utils.Math;
using Interpolation.Utils.UIExtensionClasses;
using OpenTK;

namespace Interpolation.Core
{
    public class Settings : BindableObject
    {
        private int _animationTime = 5;
        private int _displayStepsCount = 10;
        private bool _displaySteps = false;
        private Types.QuaternionRotationTypes _currentQuaternionRotationType = Types.QuaternionRotationTypes.SLERP;
        private bool _isRunning;
        private Vector3 _startPosition = Vector3.Zero;
        private Vector3 _endPosition = Vector3.Zero;
        private Quaternion _startRotationQ = Quaternion.Identity;
        private Vector3 _startRotationE = Vector3.Zero;
        private Quaternion _endRotationQ = Quaternion.Identity;
        private Vector3 _endRotationE = Vector3.Zero;
        private bool _uiEnabled = true;

        public bool UIEnabled
        {
            get { return _uiEnabled; }
            set
            {
                _uiEnabled = value; 
                RaisePropertyChanged();
            }
        }

        public int AnimationTime
        {
            get { return _animationTime; }
            set
            {
                _animationTime = value; 
                RaisePropertyChanged();
            }
        }

        public int DisplayStepsCount
        {
            get { return _displayStepsCount; }
            set
            {
                _displayStepsCount = value;
                if (_displaySteps)
                {
                    Simulation.Scene.RemoveSteps();
                    Simulation.Scene.AddSteps(_displayStepsCount);
                }
                RaisePropertyChanged();
            }
        }

        public bool DisplaySteps
        {
            get { return _displaySteps; }
            set
            {
                _displaySteps = value;
                if (_displaySteps)
                {
                    Simulation.Scene.RemoveSteps();
                    Simulation.Scene.AddSteps(_displayStepsCount);
                }
                if(!_displaySteps)
                    Simulation.Scene.RemoveSteps();
                RaisePropertyChanged();
            }
        }

        public Types.QuaternionRotationTypes CurrentQuaternionRotationType
        {
            get { return _currentQuaternionRotationType; }
            set
            {
                _currentQuaternionRotationType = value; 
                RaisePropertyChanged();
            }
        }

        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                _isRunning = value; 
                RaisePropertyChanged();
            }
        }

        public Vector3 StartPosition
        {
            get { return _startPosition; }
            set
            {
                _startPosition = value;

                if (Simulation.Scene.ESceneCursorStart != null)
                    Simulation.Scene.ESceneCursorStart.Transform.Position = _startPosition;
                if (Simulation.Scene.QSceneCursorStart != null)
                    Simulation.Scene.QSceneCursorStart.Transform.Position = _startPosition;

                RaisePropertyChanged();
            }
        }

        public Vector3 EndPosition
        {
            get { return _endPosition; }
            set
            {
                _endPosition = value;
                if (Simulation.Scene.ESceneCursorEnd != null)
                    Simulation.Scene.ESceneCursorEnd.Transform.Position = _endPosition;
                if (Simulation.Scene.QSceneCursorEnd != null)
                    Simulation.Scene.QSceneCursorEnd.Transform.Position = _endPosition;

                RaisePropertyChanged();
            }
        }

        public Quaternion StartRotationQ
        {
            get { return _startRotationQ; }
            set
            {
                _startRotationQ = value;
                _startRotationE = MathGeneralUtils.ToEulerAngles(_startRotationQ);
                RaisePropertyChanged("StartRotationE");
                if (Simulation.Scene.QSceneCursorStart != null)
                    Simulation.Scene.QSceneCursorStart.Transform.RotationQ = _startRotationQ;
                if (Simulation.Scene.ESceneCursorStart != null)
                    Simulation.Scene.ESceneCursorStart.Transform.RotationE = _startRotationE;
                RaisePropertyChanged();
            }
        }

        public Quaternion EndRotationQ
        {
            get { return _endRotationQ; }
            set
            {
                _endRotationQ = value;
                _endRotationE = MathGeneralUtils.ToEulerAngles(_endRotationQ);
                RaisePropertyChanged("EndRotationE");
                if (Simulation.Scene.QSceneCursorEnd != null)
                    Simulation.Scene.QSceneCursorEnd.Transform.RotationQ = _endRotationQ;
                if (Simulation.Scene.ESceneCursorEnd != null)
                    Simulation.Scene.ESceneCursorEnd.Transform.RotationE = _endRotationE;
                RaisePropertyChanged();
            }
        }


        public Vector3 StartRotationE
        {
            get { return _startRotationE; }
            set
            {
                _startRotationE = value;

                var startRad = new Vector3(MathHelper.DegreesToRadians(_startRotationE.X), MathHelper.DegreesToRadians(_startRotationE.Y), MathHelper.DegreesToRadians(_startRotationE.Z));
                _startRotationQ = MathGeneralUtils.ToQuaternion(startRad.Z, startRad.Y, startRad.X);
                RaisePropertyChanged("StartRotationQ");
                if (Simulation.Scene.ESceneCursorStart != null)
                    Simulation.Scene.ESceneCursorStart.Transform.RotationE = _startRotationE;
                if (Simulation.Scene.QSceneCursorStart != null)
                    Simulation.Scene.QSceneCursorStart.Transform.RotationQ = _startRotationQ;
                RaisePropertyChanged();
            }
        }

        public Vector3 EndRotationE
        {
            get { return _endRotationE; }
            set
            {
                _endRotationE = value;

                var endRad = new Vector3(MathHelper.DegreesToRadians(_endRotationE.X), MathHelper.DegreesToRadians(_endRotationE.Y), MathHelper.DegreesToRadians(_endRotationE.Z));
                _endRotationQ = MathGeneralUtils.ToQuaternion(endRad.Z, endRad.Y, endRad.X);
                RaisePropertyChanged("EndRotationQ");
                if (Simulation.Scene.ESceneCursorEnd != null)
                    Simulation.Scene.ESceneCursorEnd.Transform.RotationE = _endRotationE;
                if (Simulation.Scene.QSceneCursorEnd != null)
                    Simulation.Scene.QSceneCursorEnd.Transform.RotationQ = _endRotationQ;
                RaisePropertyChanged();
            }
        }
    }
}
