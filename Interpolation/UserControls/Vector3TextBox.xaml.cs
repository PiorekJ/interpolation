﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using OpenTK;

namespace Interpolation.UserControls
{
    /// <summary>
    /// Interaction logic for Vector3TextBox.xaml
    /// </summary>
    public partial class Vector3TextBox : UserControl, INotifyPropertyChanged
    {
        public Vector3 Value
        {
            get
            {
                return (Vector3)GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        public double IncrementValue
        {
            get
            {
                return (double)GetValue(IncrementValueProperty);
            }
            set
            {
                SetValue(IncrementValueProperty, value);
            }
        }

        public double IncrementDistance
        {
            get
            {
                return (double)GetValue(IncrementDistanceProperty);
            }
            set
            {
                SetValue(IncrementDistanceProperty, value);
            }
        }

        private float _initialValue = 0.0f;

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(nameof(Value), typeof(Vector3), typeof(Vector3TextBox), new PropertyMetadata(Vector3.Zero, OnValueChanged));

        public static readonly DependencyProperty IncrementValueProperty = DependencyProperty.Register(nameof(IncrementValue), typeof(double), typeof(Vector3TextBox), new PropertyMetadata(0.25, OnIncrementValueChanged));

        public static readonly DependencyProperty IncrementDistanceProperty = DependencyProperty.Register(nameof(IncrementDistance), typeof(double), typeof(Vector3TextBox), new PropertyMetadata(25.0, OnIncrementDistanceChanged));


        public float X
        {
            get { return (float)Math.Round(((Vector3)GetValue(ValueProperty)).X, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Vector3(value, Value.Y, Value.Z);
                OnPropertyChanged();
            }
        }

        public float Y
        {
            get { return (float)Math.Round(((Vector3)GetValue(ValueProperty)).Y, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Vector3(Value.X, value, Value.Z);
                OnPropertyChanged();
            }
        }

        public float Z
        {
            get { return (float)Math.Round(((Vector3)GetValue(ValueProperty)).Z, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Vector3(Value.X, Value.Y, value);
                OnPropertyChanged();
            }
        }

        public Vector3TextBox()
        {
            InitializeComponent();
            ((FrameworkElement)Content).DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private static void OnIncrementDistanceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Vector3TextBox control = sender as Vector3TextBox;
            if (control != null)
            {
                if (control.PropertyChanged == null)
                    return;

                control.PropertyChanged(sender, new PropertyChangedEventArgs("IncrementDistance"));
            }
        }

        private static void OnIncrementValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Vector3TextBox control = sender as Vector3TextBox;
            if (control != null)
            {
                if (control.PropertyChanged == null)
                    return;

                control.PropertyChanged(sender, new PropertyChangedEventArgs("IncrementValue"));
            }
        }

        private static void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Vector3TextBox control = sender as Vector3TextBox;
            if (control != null)
            {
                if (control.PropertyChanged == null)
                    return;

                control.PropertyChanged(sender, new PropertyChangedEventArgs("X"));
                control.PropertyChanged(sender, new PropertyChangedEventArgs("Y"));
                control.PropertyChanged(sender, new PropertyChangedEventArgs("Z"));
            }
        }

        private void XLabel_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            _initialValue = Value.X;
            XLabel.CaptureMouse();
        }

        private void YLabel_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            _initialValue = Value.Y;
            YLabel.CaptureMouse();
        }

        private void ZLabel_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            _initialValue = Value.Z;
            ZLabel.CaptureMouse();
        }

        private void Label_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            var label = (Label)sender;
            if (label.IsMouseCaptured)
            {
                label.ReleaseMouseCapture();
            }
        }

        private void XLabel_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (XLabel.IsMouseCaptured)
            {
                var pos = e.GetPosition(XLabel);
                X = _initialValue + (float)(IncrementValue * (int)(pos.X / IncrementDistance));
            }
        }

        private void YLabel_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (YLabel.IsMouseCaptured)
            {
                var pos = e.GetPosition(YLabel);
                Y = _initialValue + (float)(IncrementValue * (int)(pos.X / IncrementDistance));
            }
        }

        private void ZLabel_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (ZLabel.IsMouseCaptured)
            {
                var pos = e.GetPosition(ZLabel);
                Z = _initialValue + (float)(IncrementValue * (int)(pos.X / IncrementDistance));
            }
        }
    }
}
