﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using OpenTK;

namespace Interpolation.UserControls
{
    /// <summary>
    /// Interaction logic for QuaternionTextBox.xaml
    /// </summary>
    public partial class QuaternionTextBox : UserControl, INotifyPropertyChanged
    {
        public Quaternion Value
        {
            get
            {
                return (Quaternion)GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        public double IncrementValue
        {
            get
            {
                return (double)GetValue(IncrementValueProperty);
            }
            set
            {
                SetValue(IncrementValueProperty, value);
            }
        }

        public double IncrementDistance
        {
            get
            {
                return (double)GetValue(IncrementDistanceProperty);
            }
            set
            {
                SetValue(IncrementDistanceProperty, value);
            }
        }

        private float _initialValue = 0.0f;

        public IValueConverter CustomConverter { get; set; }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(nameof(Value), typeof(Quaternion), typeof(QuaternionTextBox), new PropertyMetadata(Quaternion.Identity, OnValueChanged));

        public static readonly DependencyProperty IncrementValueProperty = DependencyProperty.Register(nameof(IncrementValue), typeof(double), typeof(QuaternionTextBox), new PropertyMetadata(0.25, OnIncrementValueChanged));

        public static readonly DependencyProperty IncrementDistanceProperty = DependencyProperty.Register(nameof(IncrementDistance), typeof(double), typeof(QuaternionTextBox), new PropertyMetadata(25.0, OnIncrementDistanceChanged));

        public float X
        {
            get { return (float)Math.Round(((Quaternion)GetValue(ValueProperty)).X, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Quaternion(value, Value.Y, Value.Z, Value.W);
                OnPropertyChanged();
            }
        }

        public float Y
        {
            get { return (float)Math.Round(((Quaternion)GetValue(ValueProperty)).Y, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Quaternion(Value.X, value, Value.Z, Value.W);
                OnPropertyChanged();
            }
        }

        public float Z
        {
            get { return (float)Math.Round(((Quaternion)GetValue(ValueProperty)).Z, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Quaternion(Value.X, Value.Y, value, Value.W);
                OnPropertyChanged();
            }
        }

        public float W
        {
            get { return (float)Math.Round(((Quaternion)GetValue(ValueProperty)).W, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Quaternion(Value.X, Value.Y, Value.Z, value);
                OnPropertyChanged();
            }
        }


        public QuaternionTextBox()
        {
            InitializeComponent();
            ((FrameworkElement)Content).DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private static void OnIncrementDistanceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            QuaternionTextBox control = sender as QuaternionTextBox;
            if (control != null)
            {
                if (control.PropertyChanged == null)
                    return;

                control.PropertyChanged(sender, new PropertyChangedEventArgs("IncrementDistance"));
            }
        }

        private static void OnIncrementValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            QuaternionTextBox control = sender as QuaternionTextBox;
            if (control != null)
            {
                if (control.PropertyChanged == null)
                    return;

                control.PropertyChanged(sender, new PropertyChangedEventArgs("IncrementValue"));
            }
        }

        private static void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            QuaternionTextBox control = sender as QuaternionTextBox;
            if (control != null)
            {
                if (control.PropertyChanged == null)
                    return;

                control.PropertyChanged(sender, new PropertyChangedEventArgs("X"));
                control.PropertyChanged(sender, new PropertyChangedEventArgs("Y"));
                control.PropertyChanged(sender, new PropertyChangedEventArgs("Z"));
                control.PropertyChanged(sender, new PropertyChangedEventArgs("W"));
            }
        }

        private void XLabel_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            _initialValue = Value.X;
            XLabel.CaptureMouse();
        }

        private void YLabel_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            _initialValue = Value.Y;
            YLabel.CaptureMouse();
        }

        private void ZLabel_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            _initialValue = Value.Z;
            ZLabel.CaptureMouse();
        }

        private void WLabel_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            _initialValue = Value.W;
            WLabel.CaptureMouse();
        }

        private void Label_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            var label = (Label)sender;
            if (label.IsMouseCaptured)
            {
                label.ReleaseMouseCapture();
            }
        }

        private void XLabel_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (XLabel.IsMouseCaptured)
            {
                var pos = e.GetPosition(XLabel);
                X = _initialValue + (float)(IncrementValue * (int)(pos.X / IncrementDistance));
            }
        }

        private void YLabel_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (YLabel.IsMouseCaptured)
            {
                var pos = e.GetPosition(YLabel);
                Y = _initialValue + (float)(IncrementValue * (int)(pos.X / IncrementDistance));
            }
        }

        private void ZLabel_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (ZLabel.IsMouseCaptured)
            {
                var pos = e.GetPosition(ZLabel);
                Z = _initialValue + (float)(IncrementValue * (int)(pos.X / IncrementDistance));
            }
        }

        private void WLabel_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (WLabel.IsMouseCaptured)
            {
                var pos = e.GetPosition(WLabel);
                W = _initialValue + (float)(IncrementValue * (int)(pos.X / IncrementDistance));
            }
        }
    }
}
