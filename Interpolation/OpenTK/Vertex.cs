﻿using System;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Interpolation.OpenTK
{
    public interface IVertex
    {
        void Initialize();
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPC : IVertex
    {
        public Vector3 Position;
        public Vector3 Color;

        public static readonly int SizeInBytes = Marshal.SizeOf(typeof(VertexPC));
        public static readonly int PositionOffset = (int)Marshal.OffsetOf<VertexPC>(nameof(Position));
        public static readonly int ColorOffset = (int)Marshal.OffsetOf<VertexPC>(nameof(Color));

        public VertexPC(Vector3 position, Vector3 color)
        {
            Position = position;
            Color = color;
        }

        public VertexPC(float x, float y, float z, float sR, float sG, float sB)
        {
            Position = new Vector3(x, y, z);
            Color = new Vector3(sR, sG, sB);
        }

        public VertexPC(float x, float y, float z, Vector3 color)
        {
            Position = new Vector3(x, y, z);
            Color = color;
        }

        public void Initialize()
        {
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, SizeInBytes, PositionOffset);
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, SizeInBytes, ColorOffset);
        }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPN : IVertex
    {
        public Vector3 Position;
        public Vector3 Normal;

        public static readonly int SizeInBytes = Marshal.SizeOf(typeof(VertexPN));
        public static readonly int PositionOffset = (int)Marshal.OffsetOf<VertexPN>(nameof(Position));
        public static readonly int NormalOffset = (int)Marshal.OffsetOf<VertexPN>(nameof(Normal));

        public VertexPN(Vector3 position, Vector3 normal)
        {
            Position = position;
            Normal = normal;
        }

        public VertexPN(float x, float y, float z, float nx, float ny, float nz)
        {
            Position = new Vector3(x, y, z);
            Normal = new Vector3(nx, ny, nz);
        }

        public VertexPN(float x, float y, float z, Vector3 normal)
        {
            Position = new Vector3(x, y, z);
            Normal = normal;
        }

        public void Initialize()
        {
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, SizeInBytes, PositionOffset);
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, SizeInBytes, NormalOffset);
        }
    }




    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexP : IVertex
    {
        public Vector3 Position;

        public static readonly int SizeInBytes = Marshal.SizeOf(typeof(VertexP));
        public static readonly int PositionOffset = (int)Marshal.OffsetOf<VertexP>(nameof(Position));

        public VertexP(Vector3 position)
        {
            Position = position;
        }
        public VertexP(float x, float y, float z)
        {
            Position = new Vector3(x, y, z);
        }

        public void Initialize()
        {
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, SizeInBytes, PositionOffset);
        }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPT : IVertex
    {
        public Vector3 Position;
        public Vector2 TexCoords;

        public static readonly int SizeInBytes = Marshal.SizeOf(typeof(VertexPT));
        public static readonly int PositionOffset = (int)Marshal.OffsetOf<VertexPT>(nameof(Position));
        public static readonly int TexCoordsOffset = (int)Marshal.OffsetOf<VertexPT>(nameof(TexCoords));

        public VertexPT(Vector3 position, Vector2 texCoords)
        {
            Position = position;
            TexCoords = texCoords;
        }

        public VertexPT(float x, float y, float z, float tx, float ty)
        {
            Position = new Vector3(x, y, z);
            TexCoords = new Vector2(tx, ty);
        }

        public void Initialize()
        {
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, SizeInBytes, PositionOffset);
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, SizeInBytes, TexCoordsOffset);
        }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPNT : IVertex
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Vector2 TexCoords;

        public static readonly int SizeInBytes = Marshal.SizeOf(typeof(VertexPNT));
        public static readonly int PositionOffset = (int)Marshal.OffsetOf<VertexPNT>(nameof(Position));
        public static readonly int NormalOffset = (int)Marshal.OffsetOf<VertexPNT>(nameof(Normal));
        public static readonly int TexCoordsOffset = (int)Marshal.OffsetOf<VertexPNT>(nameof(TexCoords));

        public VertexPNT(Vector3 position, Vector3 normal, Vector2 texCoords)
        {
            Position = position;
            Normal = normal;
            TexCoords = texCoords;
        }

        public VertexPNT(float x, float y, float z, float nx, float ny, float nz, float tx, float ty)
        {
            Position = new Vector3(x, y, z);
            Normal = new Vector3(nx, ny, nz);
            TexCoords = new Vector2(tx, ty);
        }

        public void Initialize()
        {
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, SizeInBytes, PositionOffset);
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, SizeInBytes, NormalOffset);
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, SizeInBytes, TexCoordsOffset);

        }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPNC : IVertex
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Vector3 Color;

        public static readonly int SizeInBytes = Marshal.SizeOf(typeof(VertexPNC));
        public static readonly int PositionOffset = (int)Marshal.OffsetOf<VertexPNC>(nameof(Position));
        public static readonly int NormalOffset = (int)Marshal.OffsetOf<VertexPNC>(nameof(Normal));
        public static readonly int ColorOffset = (int)Marshal.OffsetOf<VertexPNC>(nameof(Color));

        public VertexPNC(Vector3 position, Vector3 normal, Vector3 color)
        {
            Position = position;
            Normal = normal;
            Color = color;
        }

        public VertexPNC(float x, float y, float z, float nx, float ny, float nz, float sR, float sG, float sB)
        {
            Position = new Vector3(x, y, z);
            Normal = new Vector3(nx, ny, nz);
            Color = new Vector3(sR, sG, sB);
        }

        public void Initialize()
        {
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, SizeInBytes, PositionOffset);
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, SizeInBytes, NormalOffset);
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, false, SizeInBytes, ColorOffset);

        }
    }
}
