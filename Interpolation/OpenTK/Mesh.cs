﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK.Graphics.OpenGL;

namespace Interpolation.OpenTK
{
    public enum AccessType
    {
        Static,
        Dynamic,
        Stream
    }

    public enum MeshType
    {
        Points,
        Lines,
        Triangles
    }

    public interface IMesh : IDisposable
    {
        void Draw();
        void DrawTexture(int texId);
    }

    public class Mesh<TVertex> : IMesh
        where TVertex : struct, IVertex
    {
        private PrimitiveType _meshPrimitiveType;

        private int _vao;
        private int _vbo;
        private int _ebo;

        private TVertex[] _vertices;
        private uint[] _edges;

        public Mesh(IEnumerable<TVertex> vertices, IEnumerable<uint> edges, MeshType type, AccessType accessType)
        {
            BufferUsageHint usageHint = AccessTypeToBufferUsageHint(accessType);

            if (vertices == null)
                throw new NullReferenceException("Vertices not initialized");

            _vertices = vertices.ToArray();

            if (edges != null)
                _edges = edges.ToArray();
            else
            {
                _edges = new uint[_vertices.Length];
                for (uint i = 0; i < _edges.Length; i++)
                {
                    _edges[i] = i;
                }
            }

            _vao = GL.GenVertexArray();
            _vbo = GL.GenBuffer();
            _ebo = GL.GenBuffer();


            _meshPrimitiveType = MeshTypeToPrimitiveType(type);



            GL.BindVertexArray(_vao);

            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(_vertices.Length * Marshal.SizeOf<TVertex>()), _vertices, usageHint);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(_edges.Length * sizeof(uint)), _edges, usageHint);

            TVertex vertex = new TVertex();
            vertex.Initialize();
            GL.BindVertexArray(0);
        }

        public void Draw()
        {
            GL.BindVertexArray(_vao);
            GL.DrawElements(_meshPrimitiveType, _edges.Length, DrawElementsType.UnsignedInt, IntPtr.Zero);
        }

        public void DrawTexture(int texId)
        {
            GL.BindTexture(TextureTarget.ProxyTexture2D, texId);
            GL.BindVertexArray(_vao);
            GL.DrawElements(_meshPrimitiveType, _edges.Length, DrawElementsType.UnsignedInt, IntPtr.Zero);
        }

        public void SetVertices(uint startIndex, TVertex[] vertices)
        {
            for (int i = 0; i < vertices.Length; ++i)
                _vertices[startIndex + i] = vertices[i];

            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)(startIndex * Marshal.SizeOf<TVertex>()), (IntPtr)(vertices.Length * Marshal.SizeOf<TVertex>()), vertices);
        }

        public void Dispose()
        {
            if (_vao != 0)
            {
                GL.DeleteBuffer(_vao);
                _vao = 0;
            }
            if (_vbo != 0)
            {
                GL.DeleteBuffer(_vbo);
                _vbo = 0;
            }
            if (_ebo != 0)
            {
                GL.DeleteBuffer(_ebo);
                _ebo = 0;
            }
        }


        private static BufferUsageHint AccessTypeToBufferUsageHint(AccessType type)
        {
            switch (type)
            {
                case AccessType.Dynamic:
                    return BufferUsageHint.DynamicDraw;
                case AccessType.Static:
                    return BufferUsageHint.StaticDraw;
                case AccessType.Stream:
                    return BufferUsageHint.StreamDraw;
                default:
                    throw new ArgumentOutOfRangeException("Access type not found");
            }
        }

        private static PrimitiveType MeshTypeToPrimitiveType(MeshType type)
        {
            switch (type)
            {
                case MeshType.Points:
                    return PrimitiveType.Points;
                case MeshType.Lines:
                    return PrimitiveType.Lines;
                case MeshType.Triangles:
                    return PrimitiveType.Triangles;
                default:
                    throw new ArgumentOutOfRangeException("Mesh type not found");
            }
        }
    }
}
