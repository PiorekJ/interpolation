﻿using System;
using System.IO;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Interpolation.OpenTK
{
    public class Shader : IDisposable
    {
        private int _progID;

        public Shader(params ShaderObject[] shaders)
        {
            int successFlag;
            _progID = GL.CreateProgram();
            foreach (var shaderObject in shaders)
            {
                GL.AttachShader(_progID, shaderObject.ShaderID);
            }
            GL.LinkProgram(_progID);
            GL.GetProgram(_progID, GetProgramParameterName.LinkStatus, out successFlag);
            if (successFlag == 0)
            {
                var infoLog = GL.GetProgramInfoLog(_progID);
                GL.DeleteProgram(_progID);
                throw new InvalidDataException("Program linking: " + infoLog);
            }
            foreach (var shaderObject in shaders)
            {
                GL.DetachShader(_progID, shaderObject.ShaderID);
            }
        }

        public void Use()
        {
            if (_progID != 0)
                GL.UseProgram(_progID);
        }

        public void Dispose()
        {
            if (_progID != 0)
            {
                GL.DeleteProgram(_progID);
                _progID = 0;
            }
        }

        #region Uniforms

        public int GetUniformLocation(string name)
        {
            return GL.GetUniformLocation(_progID, name);
        }

        public void Bind(int location, Matrix4 matrix)
        {
            GL.UniformMatrix4(location, false, ref matrix);
        }

        public void Bind(int location, Vector3 vector)
        {
            GL.Uniform3(location, vector);
        }

        public void Bind(int location, int integer)
        {
            GL.Uniform1(location, integer);
        }

        public void Bind(int location, float f)
        {
            GL.Uniform1(location, f);
        }
        #endregion

    }

    public class ShaderObject : IDisposable
    {
        public int ShaderID { get; private set; }

        public ShaderObject(string filepath, ShaderType type)
        {
            CreateFromFile(filepath, type);
        }

        private void CreateFromFile(string filepath, ShaderType type)
        {
            string shaderCode;
            int successFlag;
            try
            {
                shaderCode = File.ReadAllText(filepath);
            }
            catch (Exception e)
            {
                throw new Exception("Reading shader from file: ", e);
            }

            ShaderID = GL.CreateShader(type);
            GL.ShaderSource(ShaderID, shaderCode);
            GL.CompileShader(ShaderID);


            GL.GetShader(ShaderID, ShaderParameter.CompileStatus, out successFlag);
            if (successFlag == 0)
            {
                var infoLog = GL.GetShaderInfoLog(ShaderID);
                GL.DeleteShader(ShaderID);
                throw new InvalidDataException("Shader compilation: " + infoLog);
            }
        }

        public void Dispose()
        {
            if (ShaderID != 0)
            {
                GL.DeleteShader(ShaderID);
                ShaderID = 0;
            }
        }
    }

    public class VertexShaderObject : ShaderObject
    {
        public VertexShaderObject(string filepath) : base(filepath, ShaderType.VertexShader)
        {
        }
    }

    public class FragmentShaderObject : ShaderObject
    {
        public FragmentShaderObject(string filepath) : base(filepath, ShaderType.FragmentShader)
        {
        }
    }

    public class GeometricShaderObject : ShaderObject
    {
        public GeometricShaderObject(string filepath) : base(filepath, ShaderType.GeometryShader)
        {
        }
    }

}
