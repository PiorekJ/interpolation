﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Interpolation.Utils.Converters
{
    class RadianToDegreesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            float retValue = 0;
            if (value != null)
            {
                retValue = (float)value / (float)System.Math.PI * 180;
            }

            return retValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
