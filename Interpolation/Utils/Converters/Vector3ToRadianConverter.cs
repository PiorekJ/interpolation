﻿using System;
using System.Globalization;
using System.Windows.Data;
using OpenTK;
using PureCAD.Utils;

namespace Interpolation.Utils.Converters
{
    class Vector3ToRadianConverter : IValueConverter
    {
        //From Quaternion to Vector3
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                Quaternion valueQuaternion = (Quaternion)value;
                return valueQuaternion.EulerAngles();
            }
            return Vector3.Zero;

        }
        //From Vector3 to Quaternion
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                Vector3 valueVector3 = (Vector3)value;
                return new Quaternion(valueVector3.Zyx * (float)System.Math.PI / 180);
            }
            return Quaternion.Identity;
        }
    }
}
