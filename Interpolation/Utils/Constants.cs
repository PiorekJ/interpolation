﻿using System.Windows.Media;
using OpenTK;

namespace Interpolation.Utils
{
    public static class Constants
    {
        public const int LineSize = 2;

        public static Vector3 DefaultCameraPosition = new Vector3(0, 0, 5);
        public const float DefaultFOV = (float)System.Math.PI / 4;
        public const float DefaultZNear = 0.01f;
        public const float DefaultZFar = 150f;

        public const float CameraMovementMouseSensitivity = 0.5f;
        public const float CameraRotationMouseSensitivity = 0.05f;
        public const float CameraZoomMouseSensitivity = 0.5f;

        public const float CameraMovementKeyVelocity = 5.0f;
        public const float CameraMovementKeySlowVelocity = 0.25f;

        public static readonly Color BackgroundColor = (Color)ColorConverter.ConvertFromString("#FF616161");

        public static readonly float QuaternionThreshold = 0.9995f;
    }
}
