﻿using System;
using System.Collections.ObjectModel;

namespace Interpolation.Utils.UIExtensionClasses
{
    public class ObservableSet<T> : ObservableCollection<T>
    {
        protected override void InsertItem(int index, T item)
        {
            if (Contains(item))
                throw new ArgumentException("Duplicate item: " + item);

            base.InsertItem(index, item);
        }

        protected override void SetItem(int index, T item)
        {
            var idx = IndexOf(item);
            if (idx >= 0 && idx != index)
                throw new ArgumentException("Duplicate item: " + item);

            base.SetItem(index, item);
        }
    }
}
